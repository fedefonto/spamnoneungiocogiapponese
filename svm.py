import math
from sklearn import svm
from sklearn.metrics import pairwise
from sklearn.model_selection import cross_val_score


# This function transforms the dataset in the .data format into a list of lists.
def getWordArray():
	spamdatabase = open("spambase.data", "r")
	lines = spamdatabase.readlines()
	spamdatabase.close()
	listofwords = []
	for word in lines:
		word = word.replace("\r\n", "")
		wordvalue = word.split(",")
		values = []
		i = 0
		for value in wordvalue:
			if i not in [54, 55, 56]:
				values.append(float(value))
			i = i + 1

		listofwords.append(values)
	return listofwords

# This function transforms the representation of the classes (spam 1, notspam 0) into spam 1 and notspam -1.
def classes(dataset):
	classes_vector = []
	for mail in dataset:
		if mail[-1] == float(1):  # if the mail is considered as spam
			classes_vector.append(1)
		else:
			classes_vector.append(-1)
	return classes_vector


# Build an array with IDF values.
def build_idf(data):
	idf = []
	n_documents = len(data) # count the number of mails.
	n_i = len(data[0]) - 1 # number of features 
	for i in range(n_i):
		n_doc_with_i = 0.0
		for j in range(n_documents):
			if data[j][i] != 0.0: # if the percentage is not 0
				n_doc_with_i = n_doc_with_i + 1.0 # increment the number of documents with the term i
		idf.append(math.log(n_documents / n_doc_with_i)) # compute the IDF value
	return idf


# Build a matrix with TF-IDF values
def build_tfidf(data):
	idf = build_idf(data) # compute the IDF values.
	tfidf = []
	for doc in data:
		docvalue = []
		j = 0
		for tf in doc:
			if j != 54:
				docvalue.append(tf /100 * idf[j]) # TF is the dataset value divided by 100. Then we multiply by the corresponding IDF value.
			j = j + 1
		tfidf.append(docvalue)
	return tfidf 


#### MAIN ####

# phase 0: preliminary work
data = getWordArray()
d = build_tfidf(data)

# Kernel => linear
print("Kernel type: linear\n")
clf = svm.SVC(C=1.0, kernel="linear")
scores = cross_val_score(clf, [item for item in d], [item[54] for item in data], cv=10)
i = 1
for s in scores:
	print("Accuracy at {} round of cross validation: {}").format(i, s)
	i = i + 1

print("\nAccuracy mean in cross validation: %0.2f (+/- %0.2f)\n\n" % (scores.mean(), scores.std() * 2))


# Kernel => polynomial
print("Kernel type: polynomial of degree 2")
clf = svm.SVC(C=1.0, kernel='poly', degree=2)
# scores = cross_val_score(clf, spam_data_train, spam_target_train, cv=10)
scores = cross_val_score(clf, [item for item in d], [item[54] for item in data], cv=10)
i = 1
for s in scores:
	print("Accuracy at {} round of cross validation: {}").format(i, s)
	i = i + 1

print("\nAccuracy mean in cross validation: %0.2f (+/- %0.2f)\n\n" % (scores.mean(), scores.std() * 2))

# Kernel => RBF
print("Kernel type: RBF ")
clf = svm.SVC(C=1.0, kernel='rbf')
# scores = cross_val_score(clf, spam_data_train, spam_target_train, cv=10)
scores = cross_val_score(clf, [item for item in d], [item[54] for item in data], cv=10)
i = 1
for s in scores:
	print("Accuracy at {} round of cross validation: {}").format(i, s)
	i = i + 1

print("\nAccuracy mean in cross validation: %0.2f (+/- %0.2f)\n\n" % (scores.mean(), scores.std() * 2))

# Kernel => cosine similarity
print("Kernel type: cosine similarity\n")
clf = svm.SVC(C=1.0, kernel=pairwise.cosine_similarity)
scores = cross_val_score(clf, [item for item in d], [item[54] for item in data], cv=10)
i = 1
for s in scores:
	print("Accuracy at {} round of cross validation: {}").format(i, s)
	i = i + 1

print("\nAccuracy mean in cross validation: %0.2f (+/- %0.2f)\n\n" % (scores.mean(), scores.std() * 2))