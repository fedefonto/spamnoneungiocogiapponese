import numpy
import statistics
import scipy
import random
import scipy.stats

# This function transforms the dataset in the .data format into a list of lists.
def getWordArray():
	spamdatabase = open("spambase.data", "r")
	lines = spamdatabase.readlines()
	spamdatabase.close()
	listofwords = []
	for word in lines:
		word = word.replace("\r\n", "")
		wordvalue = word.split(",")
		values = []
		i = 0
		for value in wordvalue:
			if i not in [54, 55, 56]:
				values.append(float(value))
			i = i + 1

		listofwords.append(values)
	return listofwords


# This function divides the data into two different sets, spam and not_spam.
def divide_spam(data, spam, not_spam):
	for doc in data:
		if doc[-1] == 1:
			spam.append(doc)
		else:
			not_spam.append(doc)


# This function computes the parameters (mean and standard deviation) of the Gaussian distribution.
# The two parameters are computed for each set, spam and not_spam.
def mean_sigma(spam, not_spam):
	num_attributes = len(spam[0]) - 1

	spam_mean = map(statistics.mean, zip(*spam))
	not_spam_mean = map(statistics.mean, zip(*not_spam))
	spam_sigma = map(statistics.stdev, zip(*spam))
	not_spam_sigma = map(statistics.stdev, zip(*not_spam))

	return spam_mean, spam_sigma, not_spam_mean, not_spam_sigma

# This is the principal function which receives in input the two sets train and test sets and runs the algorithm.
def naive_bayes(train, test):

	spam = []
	not_spam = []

	##Training phase##

	# Divide the train set into spam and not_spam sets
	divide_spam(train, spam, not_spam)
	# Compute the a priori probabilities P(C_spam) = N_spam/N and P(C_notspam) = N_notspam/N
	not_spam_p = len(not_spam) / float(len(train))
	spam_p = len(spam) / float(len(train))

	# Call the mean_sigma function in order to calculate the mean and standard deviation values.
	values = mean_sigma(spam, not_spam)
	spam_means = values[0]
	spam_sigmas = values[1]
	not_spam_means = values[2]
	not_spam_sigmas = values[3]


	##Testing phase##

	k = 0
	classify = []

	for mail in test: # For each mail in the test set
		cond_probs_spam = []  # Here we store the probabilities P(A_i | C_spam)
		cond_probs_notspam = []  # Here we store the probabilities P(A_i | C_nonspam)

		j = 0
		for attribute in mail: # For each feature of the mail
			if j < len(mail) - 1: # Ignore the last feature, which is the classification label
				if spam_means[j] != 0 and spam_sigmas[j] != 0:
					# Compute the values of the normal distribution with the pre-computed parameters
					cond_probs_spam.append(scipy.stats.norm(spam_means[j], spam_sigmas[j]).pdf(attribute))
				if not_spam_means[j] != 0 and not_spam_sigmas[j] != 0:
					cond_probs_notspam.append(scipy.stats.norm(not_spam_means[j], not_spam_sigmas[j]).pdf(attribute))
			j = j + 1

		# We are interested in the product of sequences  P(A_i | C_spam) times P(C_spam)
		product_spam = numpy.prod(cond_probs_spam) * spam_p
		product_not_spam = numpy.prod(cond_probs_notspam) * not_spam_p

		# Compare the two probabilities: if the probability computed with the class spam is greater than the other probability class, then classify the mail as spam.
		# otherwise, classify it as not_spam.
		if product_spam > product_not_spam:
			classify.append(float(1))
		else:
			classify.append(float(0))

		# Verify if we have classified correctly.
		if mail[-1] == classify[-1]:
			k = k + 1

	print("Percentage of correctness: {}%\n".format(k / float(len(classify)) * 100))
	return k / float(len(classify)) * 100


# This function is used within the cross_validation function.
# It produces from the dataset a list of 10 folds.
def split_into_folds(dataset, folds):
	list_of_folds = []
	k = 0
	foldsize = int(len(dataset) / folds)

	for i in range(folds):
		fold = []
		for j in range(foldsize):
			fold.append(dataset[k])
			k = k + 1
		list_of_folds.append(fold)

	return list_of_folds


# This function runs the 10 way cross validation.
# Given the index of the fold to be the test set, split the 10 folds into 9 train sets and 1 test set.
def cross_validation(list_of_folds, test_idx):

	train = []
	test = []

	i = 0
	for folds in list_of_folds:
		if i == test_idx:
			for mail in folds:
				test.append(mail)
		else:
			for mail in folds:
				train.append(mail)
		i = i + 1

	# Run the algorithm giving in input the train and test sets made here.
	return naive_bayes(train, test)


#### MAIN ####

# phase 0: preliminary work.
data = getWordArray()

## Cross validation step ###
random.seed(10)
random.shuffle(data)
folds = split_into_folds(data, 10)

data = []

for i in range(10):
	print("Cross validation round {}").format(i + 1)
	data.append(cross_validation(folds, i))
print("\n\n\n Mean result: {}%").format(statistics.mean(data))


